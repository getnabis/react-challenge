# React Challenge

## Installation

Clone this repo. From the root folder, run `npm install` or `yarn`, depending on which package manager you are using.

## Running App

Run `npm run start` or `yarn start`.
