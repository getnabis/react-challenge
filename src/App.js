import React from "react";
import "./styles.css";

// https://gist.githubusercontent.com/ctram/f48838b70b1b012fd43d5ff184533232/raw/d5fce614aa257440aee28395e14e4cdfa887d051/gistfile1.json

export default function App() {
  return (
    <div className="App">
      <h1>Welcome to the Nabis code challenge!</h1>
      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
}
